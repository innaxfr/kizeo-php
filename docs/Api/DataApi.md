# kizeo\DataApi

All URIs are relative to *https://www.kizeoforms.com/rest/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**formsFormIdDataAdvancedPost**](DataApi.md#formsformiddataadvancedpost) | **POST** /forms/{formId}/data/advanced | Get List of filtered data of a form (with advanced filtering options)
[**formsFormIdDataDataIdGet**](DataApi.md#formsformiddatadataidget) | **GET** /forms/{formId}/data/{dataId} | Get data of a form
[**formsFormIdDataUnreadActionLimitGet**](DataApi.md#formsformiddataunreadactionlimitget) | **GET** /forms/{formId}/data/unread/{action}/{limit} | Get content of unread data for a given action
[**formsFormIdMarkasreadbyactionActionPost**](DataApi.md#formsformidmarkasreadbyactionactionpost) | **POST** /forms/{formId}/markasreadbyaction/{action} | Mark the data with an action
[**formsFormIdMarkasunreadbyactionActionPost**](DataApi.md#formsformidmarkasunreadbyactionactionpost) | **POST** /forms/{formId}/markasunreadbyaction/{action} | Set list of data of form to unread for a given action
[**formsFormIdPushPost**](DataApi.md#formsformidpushpost) | **POST** /forms/{formId}/push | Send push with data
[**formsPushInboxGet**](DataApi.md#formspushinboxget) | **GET** /forms/push/inbox | Receive new pushed data

# **formsFormIdDataAdvancedPost**
> \kizeo\Model\DataMin formsFormIdDataAdvancedPost($body, $form_id)

Get List of filtered data of a form (with advanced filtering options)

Get the list of filtered data of a form (with advanced filtering options), or of all forms if formId is set to 'all'

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\DataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\AdvancedFilter(); // \kizeo\Model\AdvancedFilter | Data params
$form_id = 56; // int | ID of the form

try {
    $result = $apiInstance->formsFormIdDataAdvancedPost($body, $form_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataApi->formsFormIdDataAdvancedPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\AdvancedFilter**](../Model/AdvancedFilter.md)| Data params |
 **form_id** | **int**| ID of the form |

### Return type

[**\kizeo\Model\DataMin**](../Model/DataMin.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataDataIdGet**
> \kizeo\Model\Data formsFormIdDataDataIdGet($form_id, $data_id)

Get data of a form

Get data of a form

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\DataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$data_id = 56; // int | ID of the data

try {
    $result = $apiInstance->formsFormIdDataDataIdGet($form_id, $data_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataApi->formsFormIdDataDataIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **data_id** | **int**| ID of the data |

### Return type

[**\kizeo\Model\Data**](../Model/Data.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataUnreadActionLimitGet**
> \kizeo\Model\DataMin formsFormIdDataUnreadActionLimitGet($form_id, $action, $limit)

Get content of unread data for a given action

Get the content of unread data for a given action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\DataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$action = "action_example"; // string | Action
$limit = new \kizeo\Model\null(); //  | Data chunk limit

try {
    $result = $apiInstance->formsFormIdDataUnreadActionLimitGet($form_id, $action, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataApi->formsFormIdDataUnreadActionLimitGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **action** | **string**| Action |
 **limit** | [****](../Model/.md)| Data chunk limit |

### Return type

[**\kizeo\Model\DataMin**](../Model/DataMin.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdMarkasreadbyactionActionPost**
> \kizeo\Model\DefaultResponse formsFormIdMarkasreadbyactionActionPost($body, $form_id, $action)

Mark the data with an action

Mark the data read for a given action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\DataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\DataIds(); // \kizeo\Model\DataIds | Data ids parameters
$form_id = 56; // int | ID of the form
$action = "action_example"; // string | Action

try {
    $result = $apiInstance->formsFormIdMarkasreadbyactionActionPost($body, $form_id, $action);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataApi->formsFormIdMarkasreadbyactionActionPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\DataIds**](../Model/DataIds.md)| Data ids parameters |
 **form_id** | **int**| ID of the form |
 **action** | **string**| Action |

### Return type

[**\kizeo\Model\DefaultResponse**](../Model/DefaultResponse.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdMarkasunreadbyactionActionPost**
> \kizeo\Model\DefaultResponse formsFormIdMarkasunreadbyactionActionPost($body, $form_id, $action)

Set list of data of form to unread for a given action

Set list of data of form to unread for a given action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\DataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\DataIds(); // \kizeo\Model\DataIds | Data ids parameters
$form_id = 56; // int | ID of the form
$action = "action_example"; // string | Action

try {
    $result = $apiInstance->formsFormIdMarkasunreadbyactionActionPost($body, $form_id, $action);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataApi->formsFormIdMarkasunreadbyactionActionPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\DataIds**](../Model/DataIds.md)| Data ids parameters |
 **form_id** | **int**| ID of the form |
 **action** | **string**| Action |

### Return type

[**\kizeo\Model\DefaultResponse**](../Model/DefaultResponse.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdPushPost**
> \kizeo\Model\DataMin formsFormIdPushPost($body, $form_id)

Send push with data

Send push with data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\DataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\DataPush(); // \kizeo\Model\DataPush | Data parameters
$form_id = 56; // int | ID of the form

try {
    $result = $apiInstance->formsFormIdPushPost($body, $form_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataApi->formsFormIdPushPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\DataPush**](../Model/DataPush.md)| Data parameters |
 **form_id** | **int**| ID of the form |

### Return type

[**\kizeo\Model\DataMin**](../Model/DataMin.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsPushInboxGet**
> \kizeo\Model\FormMin formsPushInboxGet()

Receive new pushed data

Receive all new pushed data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\DataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->formsPushInboxGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataApi->formsPushInboxGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\kizeo\Model\FormMin**](../Model/FormMin.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

