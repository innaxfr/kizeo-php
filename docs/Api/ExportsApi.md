# kizeo\ExportsApi

All URIs are relative to *https://www.kizeoforms.com/rest/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**formsFormIdDataDataIdExportsExportIdGet**](ExportsApi.md#formsformiddatadataidexportsexportidget) | **GET** /forms/{formId}/data/{dataId}/exports/{exportId} | Export data
[**formsFormIdDataDataIdExportsExportIdPdfGet**](ExportsApi.md#formsformiddatadataidexportsexportidpdfget) | **GET** /forms/{formId}/data/{dataId}/exports/{exportId}/pdf | Export data (PDF)
[**formsFormIdDataDataIdFormPicturesMediaNameGet**](ExportsApi.md#formsformiddatadataidformpicturesmedianameget) | **GET** /forms/{formId}/data/{dataId}/form_pictures/{mediaName} | Get one fixed image of a form
[**formsFormIdDataDataIdMediasMediaNameGet**](ExportsApi.md#formsformiddatadataidmediasmedianameget) | **GET** /forms/{formId}/data/{dataId}/medias/{mediaName} | Get one image of a form
[**formsFormIdDataDataIdPdfGet**](ExportsApi.md#formsformiddatadataidpdfget) | **GET** /forms/{formId}/data/{dataId}/pdf | Get PDF data of a form
[**formsFormIdDataMultipleCsvCustomPost**](ExportsApi.md#formsformiddatamultiplecsvcustompost) | **POST** /forms/{formId}/data/multiple/csv_custom | Get custom CSV data (multiple) of a form
[**formsFormIdDataMultipleCsvPost**](ExportsApi.md#formsformiddatamultiplecsvpost) | **POST** /forms/{formId}/data/multiple/csv | Get CSV data (multiple) of a form
[**formsFormIdDataMultipleExcelCustomPost**](ExportsApi.md#formsformiddatamultipleexcelcustompost) | **POST** /forms/{formId}/data/multiple/excel_custom | Get custom Excel list data (multiple) of a form
[**formsFormIdDataMultipleExcelPost**](ExportsApi.md#formsformiddatamultipleexcelpost) | **POST** /forms/{formId}/data/multiple/excel | Get Excel list data (multiple) of a form
[**formsFormIdExportsGet**](ExportsApi.md#formsformidexportsget) | **GET** /forms/{formId}/exports | Get list of Word and Excel exports
[**formsFormIdMultipleDataExportsExportIdPdfPost**](ExportsApi.md#formsformidmultipledataexportsexportidpdfpost) | **POST** /forms/{formId}/multiple_data/exports/{exportId}/pdf | Export data (multiple / PDF)

# **formsFormIdDataDataIdExportsExportIdGet**
> formsFormIdDataDataIdExportsExportIdGet($form_id, $data_id, $export_id)

Export data

Export one entry to the selected export.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$data_id = 56; // int | ID of the data
$export_id = 56; // int | ID of requested export model

try {
    $apiInstance->formsFormIdDataDataIdExportsExportIdGet($form_id, $data_id, $export_id);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataDataIdExportsExportIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **data_id** | **int**| ID of the data |
 **export_id** | **int**| ID of requested export model |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataDataIdExportsExportIdPdfGet**
> formsFormIdDataDataIdExportsExportIdPdfGet($form_id, $data_id, $export_id)

Export data (PDF)

Export one entry to the selected export to PDF.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$data_id = 56; // int | ID of the data
$export_id = 56; // int | ID of requested export model

try {
    $apiInstance->formsFormIdDataDataIdExportsExportIdPdfGet($form_id, $data_id, $export_id);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataDataIdExportsExportIdPdfGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **data_id** | **int**| ID of the data |
 **export_id** | **int**| ID of requested export model |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataDataIdFormPicturesMediaNameGet**
> formsFormIdDataDataIdFormPicturesMediaNameGet($form_id, $data_id, $media_name)

Get one fixed image of a form

Get one fixed image of a form

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$data_id = 56; // int | ID of the data
$media_name = "media_name_example"; // string | Name of the image

try {
    $apiInstance->formsFormIdDataDataIdFormPicturesMediaNameGet($form_id, $data_id, $media_name);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataDataIdFormPicturesMediaNameGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **data_id** | **int**| ID of the data |
 **media_name** | **string**| Name of the image |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataDataIdMediasMediaNameGet**
> formsFormIdDataDataIdMediasMediaNameGet($form_id, $data_id, $media_name)

Get one image of a form

Get one image of a form

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$data_id = 56; // int | ID of the data
$media_name = "media_name_example"; // string | Name of the image

try {
    $apiInstance->formsFormIdDataDataIdMediasMediaNameGet($form_id, $data_id, $media_name);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataDataIdMediasMediaNameGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **data_id** | **int**| ID of the data |
 **media_name** | **string**| Name of the image |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataDataIdPdfGet**
> formsFormIdDataDataIdPdfGet($form_id, $data_id)

Get PDF data of a form

Get PDF data of a form

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$data_id = 56; // int | ID of the data

try {
    $apiInstance->formsFormIdDataDataIdPdfGet($form_id, $data_id);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataDataIdPdfGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **data_id** | **int**| ID of the data |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataMultipleCsvCustomPost**
> formsFormIdDataMultipleCsvCustomPost($body, $form_id)

Get custom CSV data (multiple) of a form

Get custom CSV data (multiple) of a form

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\DataIds(); // \kizeo\Model\DataIds | Data ids Parameters
$form_id = 56; // int | ID of the form

try {
    $apiInstance->formsFormIdDataMultipleCsvCustomPost($body, $form_id);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataMultipleCsvCustomPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\DataIds**](../Model/DataIds.md)| Data ids Parameters |
 **form_id** | **int**| ID of the form |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataMultipleCsvPost**
> formsFormIdDataMultipleCsvPost($form_id, $body)

Get CSV data (multiple) of a form

Get CSV data (multiple) of a form

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$body = new \kizeo\Model\DataIds(); // \kizeo\Model\DataIds | Data ids Parameters

try {
    $apiInstance->formsFormIdDataMultipleCsvPost($form_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataMultipleCsvPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **body** | [**\kizeo\Model\DataIds**](../Model/DataIds.md)| Data ids Parameters | [optional]

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataMultipleExcelCustomPost**
> formsFormIdDataMultipleExcelCustomPost($body, $form_id)

Get custom Excel list data (multiple) of a form

Get custom Excel list data (multiple) of a form

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\DataIds(); // \kizeo\Model\DataIds | Data ids Parameters
$form_id = 56; // int | ID of the form

try {
    $apiInstance->formsFormIdDataMultipleExcelCustomPost($body, $form_id);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataMultipleExcelCustomPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\DataIds**](../Model/DataIds.md)| Data ids Parameters |
 **form_id** | **int**| ID of the form |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdDataMultipleExcelPost**
> formsFormIdDataMultipleExcelPost($body, $form_id)

Get Excel list data (multiple) of a form

Get Excel list data (multiple) of a form

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\DataIds(); // \kizeo\Model\DataIds | Data ids Parameters
$form_id = 56; // int | ID of the form

try {
    $apiInstance->formsFormIdDataMultipleExcelPost($body, $form_id);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdDataMultipleExcelPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\DataIds**](../Model/DataIds.md)| Data ids Parameters |
 **form_id** | **int**| ID of the form |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdExportsGet**
> formsFormIdExportsGet($form_id)

Get list of Word and Excel exports

Get the complete list of Word et Excel exports

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form

try {
    $apiInstance->formsFormIdExportsGet($form_id);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdExportsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **formsFormIdMultipleDataExportsExportIdPdfPost**
> formsFormIdMultipleDataExportsExportIdPdfPost($form_id, $export_id, $body)

Export data (multiple / PDF)

Export multiple entries to the selected export to PDF.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\ExportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$form_id = 56; // int | ID of the form
$export_id = 56; // int | ID of requested export model
$body = new \kizeo\Model\DataIds(); // \kizeo\Model\DataIds | Data ids Parameters

try {
    $apiInstance->formsFormIdMultipleDataExportsExportIdPdfPost($form_id, $export_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling ExportsApi->formsFormIdMultipleDataExportsExportIdPdfPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_id** | **int**| ID of the form |
 **export_id** | **int**| ID of requested export model |
 **body** | [**\kizeo\Model\DataIds**](../Model/DataIds.md)| Data ids Parameters | [optional]

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

