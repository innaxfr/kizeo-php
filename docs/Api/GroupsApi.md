# kizeo\GroupsApi

All URIs are relative to *https://www.kizeoforms.com/rest/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groupGroupIdDelete**](GroupsApi.md#groupgroupiddelete) | **DELETE** /group/{groupId} | Delete a group
[**groupGroupIdGet**](GroupsApi.md#groupgroupidget) | **GET** /group/{groupId} | Get a group
[**groupGroupIdLeaderLeaderIdDelete**](GroupsApi.md#groupgroupidleaderleaderiddelete) | **DELETE** /group/{groupId}/leader/{leaderId} | Remove a leader from a specific group
[**groupGroupIdLeaderLeaderIdPost**](GroupsApi.md#groupgroupidleaderleaderidpost) | **POST** /group/{groupId}/leader/{leaderId} | Add a leader into a specific group
[**groupGroupIdLeadersGet**](GroupsApi.md#groupgroupidleadersget) | **GET** /group/{groupId}/leaders | Get all groups&#x27; leaders of a specific group
[**groupGroupIdLeadersPost**](GroupsApi.md#groupgroupidleaderspost) | **POST** /group/{groupId}/leaders | Add several leaders into a group
[**groupGroupIdPut**](GroupsApi.md#groupgroupidput) | **PUT** /group/{groupId} | Update a group
[**groupGroupIdUserUserIdDelete**](GroupsApi.md#groupgroupiduseruseriddelete) | **DELETE** /group/{groupId}/user/{userId} | Remove an user from a specific group
[**groupGroupIdUserUserIdPost**](GroupsApi.md#groupgroupiduseruseridpost) | **POST** /group/{groupId}/user/{userId} | Add an user into a specific group
[**groupGroupIdUsersGet**](GroupsApi.md#groupgroupidusersget) | **GET** /group/{groupId}/users | Get all users of a specific group
[**groupGroupIdUsersPost**](GroupsApi.md#groupgroupiduserspost) | **POST** /group/{groupId}/users | Add several users into a group
[**groupPost**](GroupsApi.md#grouppost) | **POST** /group | Create a new group
[**groupsGet**](GroupsApi.md#groupsget) | **GET** /groups | Get all groups
[**groupsOrderedGet**](GroupsApi.md#groupsorderedget) | **GET** /groups/ordered | Get all groups ordered

# **groupGroupIdDelete**
> groupGroupIdDelete($group_id)

Delete a group

Delete existing group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = 56; // int | Id of the group

try {
    $apiInstance->groupGroupIdDelete($group_id);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Id of the group |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdGet**
> \kizeo\Model\GroupComplete groupGroupIdGet($group_id)

Get a group

Get all informations about the group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = 56; // int | Id of the group

try {
    $result = $apiInstance->groupGroupIdGet($group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Id of the group |

### Return type

[**\kizeo\Model\GroupComplete**](../Model/GroupComplete.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdLeaderLeaderIdDelete**
> \kizeo\Model\GroupComplete groupGroupIdLeaderLeaderIdDelete($group_id, $leader_id)

Remove a leader from a specific group

Remove a leader from a specific group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = 56; // int | Id of the group
$leader_id = 56; // int | Id of the leader

try {
    $result = $apiInstance->groupGroupIdLeaderLeaderIdDelete($group_id, $leader_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdLeaderLeaderIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Id of the group |
 **leader_id** | **int**| Id of the leader |

### Return type

[**\kizeo\Model\GroupComplete**](../Model/GroupComplete.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdLeaderLeaderIdPost**
> \kizeo\Model\GroupComplete groupGroupIdLeaderLeaderIdPost($group_id, $leader_id)

Add a leader into a specific group

Add a leader into a specific group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = 56; // int | Id of the group
$leader_id = 56; // int | Id of the leader

try {
    $result = $apiInstance->groupGroupIdLeaderLeaderIdPost($group_id, $leader_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdLeaderLeaderIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Id of the group |
 **leader_id** | **int**| Id of the leader |

### Return type

[**\kizeo\Model\GroupComplete**](../Model/GroupComplete.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdLeadersGet**
> \kizeo\Model\GroupUser groupGroupIdLeadersGet($group_id)

Get all groups' leaders of a specific group

Use to get all groups' leaders of a specific group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = 56; // int | Id of the group

try {
    $result = $apiInstance->groupGroupIdLeadersGet($group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdLeadersGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Id of the group |

### Return type

[**\kizeo\Model\GroupUser**](../Model/GroupUser.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdLeadersPost**
> \kizeo\Model\User groupGroupIdLeadersPost($body, $group_id)

Add several leaders into a group

Use to add several leaders into a group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\UserResponse(); // \kizeo\Model\UserResponse | Leaders ids
$group_id = 56; // int | Id of the group

try {
    $result = $apiInstance->groupGroupIdLeadersPost($body, $group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdLeadersPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\UserResponse**](../Model/UserResponse.md)| Leaders ids |
 **group_id** | **int**| Id of the group |

### Return type

[**\kizeo\Model\User**](../Model/User.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdPut**
> groupGroupIdPut($body, $group_id)

Update a group

Update existing group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\GroupModify(); // \kizeo\Model\GroupModify | Group Parameters
$group_id = 56; // int | Id of the group

try {
    $apiInstance->groupGroupIdPut($body, $group_id);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\GroupModify**](../Model/GroupModify.md)| Group Parameters |
 **group_id** | **int**| Id of the group |

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdUserUserIdDelete**
> \kizeo\Model\GroupComplete groupGroupIdUserUserIdDelete($group_id, $user_id)

Remove an user from a specific group

Remove an user from a specific group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = 56; // int | Id of the group
$user_id = 56; // int | Id of the user

try {
    $result = $apiInstance->groupGroupIdUserUserIdDelete($group_id, $user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdUserUserIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Id of the group |
 **user_id** | **int**| Id of the user |

### Return type

[**\kizeo\Model\GroupComplete**](../Model/GroupComplete.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdUserUserIdPost**
> \kizeo\Model\GroupComplete groupGroupIdUserUserIdPost($group_id, $user_id)

Add an user into a specific group

Add an user into a group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = 56; // int | Id of the group
$user_id = 56; // int | Id of the user

try {
    $result = $apiInstance->groupGroupIdUserUserIdPost($group_id, $user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdUserUserIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Id of the group |
 **user_id** | **int**| Id of the user |

### Return type

[**\kizeo\Model\GroupComplete**](../Model/GroupComplete.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdUsersGet**
> \kizeo\Model\GroupUser groupGroupIdUsersGet($group_id)

Get all users of a specific group

Use to get all users of the requested group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = 56; // int | Id of the group

try {
    $result = $apiInstance->groupGroupIdUsersGet($group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdUsersGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Id of the group |

### Return type

[**\kizeo\Model\GroupUser**](../Model/GroupUser.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupGroupIdUsersPost**
> \kizeo\Model\GroupResponse groupGroupIdUsersPost($body, $group_id)

Add several users into a group

Use to add several users into a group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\UserResponse(); // \kizeo\Model\UserResponse | Users ids
$group_id = 56; // int | Id of the group

try {
    $result = $apiInstance->groupGroupIdUsersPost($body, $group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupGroupIdUsersPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\UserResponse**](../Model/UserResponse.md)| Users ids |
 **group_id** | **int**| Id of the group |

### Return type

[**\kizeo\Model\GroupResponse**](../Model/GroupResponse.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupPost**
> \kizeo\Model\GroupResponse groupPost($body)

Create a new group

Use to a create group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \kizeo\Model\Group(); // \kizeo\Model\Group | Group Parameters

try {
    $result = $apiInstance->groupPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\kizeo\Model\Group**](../Model/Group.md)| Group Parameters |

### Return type

[**\kizeo\Model\GroupResponse**](../Model/GroupResponse.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupsGet**
> groupsGet()

Get all groups

Get Groups Information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->groupsGet();
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **groupsOrderedGet**
> \kizeo\Model\Group groupsOrderedGet()

Get all groups ordered

Use to get all groups ordered

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure API key authorization: authentication
$config = kizeo\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = kizeo\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new kizeo\Api\GroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->groupsOrderedGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->groupsOrderedGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\kizeo\Model\Group**](../Model/Group.md)

### Authorization

[authentication](../../README.md#authentication)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

