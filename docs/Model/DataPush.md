# DataPush

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipient_user_id** | **int** |  | [optional] 
**fields** | [**\kizeo\Model\FieldPush**](FieldPush.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

