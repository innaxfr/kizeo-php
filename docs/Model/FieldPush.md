# FieldPush

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field_id** | [**\kizeo\Model\FieldPushValue**](FieldPushValue.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

