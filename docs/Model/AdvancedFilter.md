# AdvancedFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**global_filters** | **string** |  | [optional] 
**filters** | [**\kizeo\Model\AdvancedFilterFilters[]**](AdvancedFilterFilters.md) |  | [optional] 
**order** | [**\kizeo\Model\AdvancedFilterOrders[]**](AdvancedFilterOrders.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

