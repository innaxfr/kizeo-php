# DataData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**form_id** | **int** |  | [optional] 
**user_id** | **int** |  | [optional] 
**create_time** | **string** |  | [optional] 
**answer_time** | **string** |  | [optional] 
**fields** | [**\kizeo\Model\FieldValue[]**](FieldValue.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

